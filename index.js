console.log('Hello World');


let player = [{
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log('Result of talk method');
		console.log("Pikachu I Choose you!")
	}
},];
console.log(player);
console.log('Result of Dot notation: ')
console.log(player[0].name);
console.log('Result of square braket notation:');
console.log(player[0].pokemon);


class Pokemon {
	constructor(name, level, health, attack, tackle){
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;
		this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));
	}
	}
}

let newPokemon1 = new Pokemon('Pikachu', '12', '24', '12');
let newPokemon2 = new Pokemon('Geodude', '8', '16', '8');
let newPokemon3 = new Pokemon('Mewtwo', '100', '200', '100');

console.log(newPokemon1);
console.log(newPokemon2);
console.log(newPokemon3);

function fightPokemon(name, level,){

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;


	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));
		target.health = target.health - this.attack;

		if (target.health <= 0){
			console.log(target.name + ' Fainted!')
		}

	}

	
}

let Pikachu = new fightPokemon('Pikachu', 12);
let Geodude = new fightPokemon('Geodude', 8);
let Mewtwo = new fightPokemon('Mewtwo', 100);

Pikachu.tackle(Geodude);
console.log(Geodude);